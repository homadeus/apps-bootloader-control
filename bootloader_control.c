/*
 * Homadeus Bootloader Control
 * Copyright (C) 2013 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <homadeus/bootloader/bootloader.h>
#include <spi/flash.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <util/crc16.h>

#include <stdio.h>
#include <string.h>

void bootloader_status_read(struct bootloader_status *config) {
    eeprom_read_block(config, EEPROM_END - sizeof(*config), sizeof(*config));
    if (crc16(CRC16_INIT, config, bootloader_config_size(config)) != config->crc16) {
        memset(config, 0, bootloader_config_size(config));
        config->valid = 0;
    }
}

void bootloader_status_write(struct bootloader_status *config) {
    config->crc16 = crc16(CRC16_INIT, config, bootloader_config_size(config));
    eeprom_write_block(config,  EEPROM_END - sizeof(*config), sizeof(*config));
}

void bootloader_status_clear(struct bootloader_status* status) {
    status->write_user_pgm = 0;
    status->write_failsafe_pgm = 0;
    status->valid = 1;
}

void bootloader_status_ok() {
    struct bootloader_status status;
    bootloader_status_read(&status);
    if (status.user_reset_nr > 1)
        status.user_reset_nr--;
    if (status.watchdog_reset_nr > 1)
        status.watchdog_reset_nr--;

    bootloader_status_write(&status);
}

int bootloader_firmware_dump_to_recovery(struct spi_flash *flash) {

    uint32_t base = RECOVERY_BASE_ADDRESS(flash);
    uint32_t fw_offset = PROGRAM_OFFSET(flash, base);

    uint16_t crc16_pgm = CRC16_INIT;
    uint16_t crc16_flash = CRC16_INIT;

    uint8_t buffer[SPM_PAGESIZE];
    bootloader_image image = { 0 };

    printf("f->size:        0x%08lx\n", flash->size);
    printf("f->sector_size: 0x%08lx\n", flash->sector_size);
    printf("FLASHEND:       0x%08lx\n", FLASHEND);
    printf("f->name:          %08s\n",  flash->name);
    printf("base:           0x%08lx\n", base);
    printf("fw_size:        0x%08lx\n", PROGRAM_SIZE);
    printf("fw_offset:      0x%08lx\n", fw_offset);

    for (uint32_t address = 0; address < PROGRAM_SIZE; address += SPM_PAGESIZE) {

        printf("Dumping: page: %lu\n", address / SPM_PAGESIZE);

        for (uint16_t i = 0; i < SPM_PAGESIZE; i++ ) {
            buffer[i] = pgm_read_byte_far(address + i);
        }
        crc16_pgm = crc16(crc16_pgm, buffer, SPM_PAGESIZE);
        printf("CRC: PGM: %04x\n", crc16_pgm);

        if (((fw_offset + address) % flash->sector_size) == 0) {
            wdt_reset();
            printf("Erasing 0x%08lx\n", fw_offset + address);
            if (spi_flash_erase(flash, fw_offset + address, flash->sector_size)) {
                goto error;
            }
        }

        wdt_reset();
        if (spi_flash_write(flash, fw_offset + address, SPM_PAGESIZE, buffer)) {
            goto error;
        }

        memset(buffer, 0, SPM_PAGESIZE);

        wdt_reset();
        if (spi_flash_read(flash, fw_offset + address, SPM_PAGESIZE, buffer)) {
            goto error;
        }

        crc16_flash = crc16(crc16_flash, buffer, SPM_PAGESIZE);

        if (crc16_pgm != crc16_flash) {
            printf("CRC: Error: %04x <> %04x\n", crc16_pgm, crc16_flash);
            goto error;
        }
    }

    image.fw_length = PROGRAM_SIZE;
    image.fw_type = FAILSAFE_FIRMWARE;
    image.crc16_saved = crc16_pgm;
    image.crc16_read = crc16_flash;

    if (spi_flash_erase(flash, base, flash->sector_size)) {
        printf("spi_flash_erase error\n");
        goto error;
    }
    if (spi_flash_write(flash, base, sizeof(image), &image)) {
        printf("spi_flash_write error\n");
        goto error;
    }
    printf("0x%04x - 0x%04x\r\n", image.crc16_saved, image.crc16_read);
    return 0;
error:
    printf("Pwet");
    return 1;
}

#if !defined(HAVE_CRC16)
uint16_t crc16(uint16_t init, void *data, size_t len) {
    uint8_t *ptr = data;

    while(len > 0) {
        init = _crc16_update(init, *ptr++);
        len--;
    }

    return init;
}
#endif // !defined(HAVE_CRC16)

