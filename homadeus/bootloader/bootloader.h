/*
 * Homadeus Bootloader Control
 * Copyright (C) 2013 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOMADEUS_BOOTLOADER_H
#define HOMADEUS_BOOTLOADER_H

#include <stdint.h>
#include <stdlib.h>

#include <spi/flash.h>
#include <homadeus/common/common.h>

#define CRC16_INIT 0xFFFF
#define EEPROM_END ((void*)E2END)
#define BOOTLDR_SIZE 0x1000

#define BOOTLOADER_FLASH_BUFFER SPM_PAGESIZE
#define FIRMWARE_SIZE(f) (FLASHEND + 1 + E2END + 1 + f->sector_size)
#define PROGRAM_SIZE (FLASHEND + 1 - BOOTLDR_SIZE)
#define PROGRAM_OFFSET(f, base) (base + f->sector_size)
#define RECOVERY_BASE_ADDRESS(f) (f->size - FIRMWARE_SIZE(f))
#define FIRMWARE_BASE_ADDRESS(f) (f->size - FIRMWARE_SIZE(f) * 2)

#define bootloader_config_size(s) ((sizeof(*s) - sizeof(s->crc16)))

struct __packed bootloader_status {
    uint8_t valid                :1;
    uint8_t user_reset_nr        :3;
    uint8_t watchdog_reset_nr    :3;
    uint8_t write_user_pgm       :1;
    uint8_t write_failsafe_pgm   :1;
    uint8_t dump_firmware        :1;
    uint64_t unused              :38;
    uint16_t crc16;
};

enum bootloader_fw {
    INVALID_FIRMWARE         = 0,
    USER_FIRMWARE,
    FAILSAFE_FIRMWARE,
};

typedef struct __packed bootloader_image {
    uint32_t           fw_length;
    uint32_t           fw_offset;
    uint16_t           crc16_saved;
    uint16_t           crc16_read;
    uint8_t            fw_type;
} bootloader_image;


int bootloader_firmware_dump_to_recovery(struct spi_flash *flash);
void bootloader_status_write(struct bootloader_status *config);
void bootloader_status_read(struct bootloader_status *config);
void bootloader_status_clear(struct bootloader_status* status);
void bootloader_status_ok();

#if !defined(HAVE_CRC16)
uint16_t crc16(uint16_t init, void *data, size_t len);
#endif // !defined(HAVE_CRC16)

#endif /* end of include guard: HOMADEUS_BOOTLOADER_H */
